# Seminar Offenburg SS2020

The ansible playbooks in this repo bootstraps an ubuntu server to be either a vpn gateway or a tor gateway.  

Requirements:  
- fresh installed ubuntu box  
- root access (password will be asked for)  
- 2 network interfaces  
  - assumed default: `ens33` primary; `ens37` secondary  

The steps included:  
- update && upgrade OS  
- enable ipv4 forwarding  
- install & configure tor/vpn (based on your choice)  

## usage
- adjust file: hosts  
- adjust variables file:  
  - `host_vars/tor-gw/vars.yml` for tor-gateway  
  - `host_vars/vpn-gw/vars.yml` for vpn-gateway  
- for vpn-gateway:  
  - place provided ovpn file in `playbooks/roles/vpn-gateway/files/`  
- run for tor gateway (substitude 'user' with your username):  
  `ansible-playbook -i hosts playbooks/tor-gateway.yml -u=user -K -k`  
- run for vpn gateway (substitude 'user' with your username):  
  `ansible-playbook -i hosts playbooks/vpn-gateway.yml -u=user -K -k`  
